# Enhance Your Kubernetes CI/CD Pipelines with GitLab & Open Source

This project contains sample code related to my talk at GitLab Commit San Francisco.

This repo contains a GitLab Web Application Firewall sample.

Test with `<script>alert("Hello!");</script>`.

Check logs:
```
kubectl -n gitlab-managed-apps exec -it $(kubectl get pods -n gitlab-managed-apps | grep 'ingress-controller' | awk '{print $1}') -- tail -f /var/log/modsec/audit.log
```

> Credits to [Sam Kerr](https://gitlab.com/stkerr/sams-express-example) for his demo project.
